# -*- coding: utf-8 -*-
import click
import pandas as pd
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
from sklearn.model_selection import train_test_split


def make_features(data, max_lag, rolling_mean_size):
    data_features = data.copy()
    data_features['day'] = data.index.day
    data_features['dayofweek'] = data.index.dayofweek
    data_features['hour'] = data.index.hour

    for lag in range(1, max_lag + 1):
        data_features['lag_{}'.format(lag)] = data_features['num_orders'].shift(lag)

    data_features['rolling_mean'] = data_features['num_orders'].shift().rolling(rolling_mean_size).mean()

    return data_features


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def main(input_filepath, output_filepath):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')
    data = pd.read_csv(input_filepath, index_col=[0], parse_dates=[0])
    data_features = make_features(data, 15, 9)
    train, test = train_test_split(data_features, shuffle=False, test_size=0.1)
    train = train.dropna()
    train.to_csv(output_filepath + "/train.csv")
    test.to_csv(output_filepath + "/test.csv")


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
