import os
import click
import logging
import pandas as pd
from catboost import CatBoostRegressor
from pathlib import Path
from dotenv import find_dotenv, load_dotenv


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.option('--model', type=click.Path(exists=True), default='catboost_model', help='CatBoost model file to use.')
def main(model: str, input_filepath: str):
    """
    Makes predictions using a trained CatBoost model.
    """
    # Load the test data
    features_test = pd.read_csv(os.path.join(input_filepath, "train.csv"),  index_col=[0], parse_dates=[0])

    # Load the CatBoost model
    catboost = CatBoostRegressor()
    catboost.load_model(model)

    # Make predictions
    results = catboost.predict(features_test)

    # Save results (you might want to customize this part)
    output_path = Path(input_filepath) / "predictions.csv"
    pd.DataFrame(results, columns=['predictions']).to_csv(output_path, index=False)

    logging.info(f"Predictions saved to {output_path}")


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
