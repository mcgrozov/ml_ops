import os
import click
import logging
import pandas as pd
import numpy as np
import mlflow
import mlflow.pyfunc
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
from catboost import CatBoostRegressor


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.option('--experiment-name', type=str, default='CatBoost Training', help='MLflow experiment name.')
@click.option('--run-name', type=str, default=None, help='MLflow run name.')
def main(input_filepath, experiment_name, run_name):
    """
    Trains a CatBoost model and logs to MLflow.
    """
    logger = logging.getLogger(__name__)
    logger.info('Starting model training')

    # Set up MLflow tracking server
    os.environ["MLFLOW_EXPERIMENT_NAME"] = experiment_name
    os.environ["MLFLOW_RUN"] = run_name

    # Load data
    train = pd.read_csv(os.path.join(input_filepath, "train.csv"),  index_col=[0], parse_dates=[0])
    test = pd.read_csv(os.path.join(input_filepath, "test.csv"),  index_col=[0], parse_dates=[0])

    # Separate features and target
    features_train = train.drop('num_orders', axis=1)
    target_train = train['num_orders']
    features_test = test.drop('num_orders', axis=1)
    target_test = test['num_orders']

    # Define and fit the model
    params = {'depth': 2, 'learning_rate': 0.1, 'l2_leaf_reg': 0.5, 'iterations': 1200}
    model = CatBoostRegressor(**params, loss_function='RMSE')
    model.fit(features_train, target_train, verbose=0)
    try:
        model.save_model("models/catboost_model")
    except Exception as e:
        print(f"Error saving model: {e}")
        print(f"Current working directory: {os.getcwd()}")
        print(f"Directory contents: {os.listdir()}")

    # Log model in MLflow
    mlflow.set_experiment(experiment_name)
    with mlflow.start_run(run_name=run_name) as run:
        # Log parameters
        mlflow.log_params(params)

        # Evaluate and log metrics
        predictions = model.predict(features_test)
        rmse = np.sqrt(((predictions - target_test) ** 2).mean())
        mlflow.log_metric("rmse", rmse)

        # Log model
        mlflow.sklearn.log_model(model, "model")

        # Register model
        mlflow.register_model(
            model_uri=f"runs:/{run.info.run_id}/model",
            name=f"catboost_{experiment_name}_{run_name}"
        )

        logger.info(f"Model registered as catboost_{experiment_name}_{run_name}")


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
